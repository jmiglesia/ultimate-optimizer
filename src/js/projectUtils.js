var renderTable = function(table){
    var htmlTable = "<table class='table'>";
    for(var i = 0; i < table.length; i++){
        htmlTable += "<tr>";
        for(var j = 0; j<table[i].length; j++){
            htmlTable = htmlTable + "<td>" + table[i][j] + "</td>";
        }
        htmlTable += "</tr>";
    }
    htmlTable += "</table>";
    return htmlTable;
}

var createTableauHTML = function(table){
    var htmlTable = "<table class='table'>";
    
    htmlTable += "<thead>";
    htmlTable += "<tr>";
    
    for(var i = 0; i < res.headerVariables.length; i++){
        htmlTable = htmlTable + "<th>" + res.headerVariables[i] + "</th>";
    }

    htmlTable += "</tr>";
    htmlTable += "<thead>";
    htmlTable += "<tbody>";
    
    for(var i = 0; i < table.length; i++){
        htmlTable += "<tr>";
        for(var j = 0; j < table[i].length; j++){
            htmlTable = htmlTable + "<td>" + table[i][j] + "</td>";
        }
        htmlTable += "</tr>";
    }

    htmlTable += "</tbody>";
    htmlTable += "</table>";
    return htmlTable;
};

var createBasicSolutionHTML = function(table){
    var htmlTable = "<table class='table'>";
    
    htmlTable += "<thead>";
    htmlTable += "<tr>";
    htmlTable = htmlTable + "<th>variables</th><th>answer</th>";
    htmlTable += "</tr>";
    htmlTable += "<thead>";
    htmlTable += "<tbody>";
    
    for(var i = 0; i < res.basicSolution[pointer].length; i++){
        htmlTable += "<tr />";
        for(var j = 0; j < res.basicSolution[pointer][i].length; j++){
            htmlTable = htmlTable + "<td> " + res.basicSolution[pointer][i][j] + "</td>";
        }
        htmlTable += "</tr>";
    }            

    htmlTable += "</tbody>";
    htmlTable += "</table>";
    return htmlTable;
};

var createMatrix = function(objectiveFunction, constraintFunctions, variables){
    var matrix = [];
    for(var j = 0; j < constraintFunctions.length; j++){
        var row = [];
        for(var i = 0; i < variables.length; i++){    
            var index = constraintFunctions[j].coeffVar.indexOf(variables[i]);
            if(index == -1){
                row[row.length] = 0;    
            } else {
                row[row.length] = constraintFunctions[j].coeff[index];
            }
        }
        matrix[matrix.length] = row;
    }

    row = [];
    for(var i = 0; i < variables.length; i++){    
        index = objectiveFunction.coeffVar.indexOf(variables[i]);
        if(index == -1){
            row[row.length] = 0;    
        } else {
            row[row.length] = objectiveFunction.coeff[index];
        }
    }

    matrix[matrix.length] = row;
    return matrix;
};

var cleanMatrix = function(matrixParam, variables){
    var matrix = [];
    for(var i = 0; i < matrixParam.length; i++ ){
        var c = 0;
        
        for(var j = 0; j < matrixParam[i].length-1; j++){
            if(matrixParam[i][j] == 0){
                c++;
            }
        }
        
        if( i == matrixParam.length-1 || (c != matrixParam[i].length-1 && matrixParam[i][matrixParam[i].length-1] != 0)){
            matrix.push(matrixParam[i]);
        }
    }
   return matrix;
};

var transposeMatrix = function(a) {
    return Object.keys(a[0]).map(function (c) {
        return a.map(function (r) {
            return r[c];
        });
    });
};

var createAnswerHTML = function(){
    var htmlTable = "<table class='table'>";
    
    htmlTable += "<thead>";
    htmlTable += "<tr>";
    htmlTable = htmlTable + "<th>variables</th><th>answer</th>";
    htmlTable += "</tr>";
    htmlTable += "<thead>";
    htmlTable += "<tbody>";
    
    for(var i = 0; i < res.variables.length; i++){
        htmlTable += "<tr />";
        for(var j = 0; j < res.basicSolution[res.basicSolution.length-1].length; j++){
            if(res.variables[i] == res.basicSolution[res.basicSolution.length-1][j][0]){
                htmlTable = htmlTable + "<td> " + res.variables[i] + "</td>";
                htmlTable = htmlTable + "<td> " + res.basicSolution[res.basicSolution.length-1][j][1] + "</td>";
            }
        }

        htmlTable += "</tr>";
    }            

    htmlTable = htmlTable + "<tr><td> z</td>";
    htmlTable = htmlTable + "<td> " + res.basicSolution[res.basicSolution.length-1][res.basicSolution[0].length-1][res.basicSolution[0][0].length-1] + "</td></tr>";
        

    htmlTable += "</tbody>";
    htmlTable += "</table>";
    return htmlTable;

};

var hasDuplicate = function(arr){
    for(var i = 0; i < arr.length; i++){
        if(arr.indexOf(arr[i]) != arr.lastIndexOf(arr[i])){
            return true;
        }
    }
    return false;
};


var copyMatrix = function(matrixParam){
    var copy = [];
    for(var i = 0; i < matrixParam.length; i++){
        var r = [];
        for(var j = 0; j < matrixParam[i].length; j++){
            r.push(matrixParam[i][j]);
        }
        copy.push(r);
    }
    return copy;
};

var normalize =  function(matrixParam){
    var matrix = copyMatrix(matrixParam);
    for(var i = 0; i < matrix.length-1; i++ ){
        if( matrix[i][matrix[i].length-1] < 0){
            for(var j = 0; j < matrix[i].length; j++){
                if(matrix[i][j] != 0){
                    matrix[i][j] *= -1;
                }
            }
        }
    }

    return matrix;
};