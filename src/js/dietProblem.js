var foodIdList = [];

var initializeTable = function(){
    foodList = lib.query("nutritionalValues");
    length  = foodList.length;

    for(i = 0; i < length; i++){
        component = "<tr id=\""+foodList[i].ID+"\"> <td> <button class=\"btn btn-success btn-circle add-button\" onclick= \"addFoodToList(" + (i+1) + ", $(this))\"><i class=\"glyphicon glyphicon-ok\"></i></button> <button class=\"btn btn-danger btn-circle remove-button\" onclick= \"removeFoodFromList(" + (i+1) + ", $(this))\"><i class=\"glyphicon glyphicon-remove\"></i></button> </td> <td>" + foodList[i].foods + "</td><td>" + foodList[i].pricePerServing + "</td><td>" + foodList[i].servingSize +"</td><td>" + foodList[i].calories + "</td><td>" + foodList[i].cholesterol + "</td><td>" + foodList[i].totalFat + "</td><td>" + foodList[i].sodium + "</td><td>" + foodList[i].carbohydrates + "</td><td>" + foodList[i].dietaryFiber +"</td><td>" + foodList[i].protein + "</td><td>" + foodList[i].vitAIU + "</td><td>" + foodList[i].vitCIU + "</td><td>" + foodList[i].calcium +"</td><td>" + foodList[i].iron + "</td></tr>";
        $("#nutritionalValuesTable tbody").append(component);
    }
};

var addFoodToList = function(id, thisComponent){
    thisComponent.hide();
    thisComponent.next().show();

    if(foodIdList.indexOf(id) != -1) return;

    foodIdList.push(id);
    
    var food = lib.query("nutritionalValues", {ID : id});

    component =  "<div foodId=\"" + id +"\" class=\"col-lg-4 "+ id +" \"><div class=\"panel panel-primary\"><div class=\"panel-heading\"><h3 class=\"panel-title panel-collapsed clickable\">" + food[0].foods + "</h3><span class=\"pull-right clickable remove-item\" onclick = \"removeItem("+id+")\"><i class=\"glyphicon glyphicon-remove\"></i></span></div><div class=\"panel-body\">Size per Serving : " + food[0].servingSize + "<br />" + "Price : $" + food[0].pricePerServing + "</td></div></div></div>";

    $("#listFoodItems").append(component);
    $("#listFoodItems ." + id).find('.panel-body').hide();
    $("#foodCount").html(foodIdList.length);
    $("#optimizeButton").removeAttr("disabled");
};


var removeElement =function(list, element){
    var newList = [];
    for(var i = 0; i < list.length; i++){
        if(list[i] != element){
            newList.push(list[i]);
        }
    }
    return newList;
};

var removeFoodFromList = function(id, thisComponent){
    foodIdList = removeElement(foodIdList, id);
    thisComponent.hide();
    thisComponent.prev().show();

    $("#listFoodItems ." + id).remove();
    $("#foodCount").html(foodIdList.length);
    
    if(foodIdList.length == 0){
        $("#optimizeButton").attr("disabled", "disabled");
    }
};

var removeItem = function(id){
    $("tbody #"+id+" .remove-button").click();
};

var redirectToSolve = function(){
    var urlParam = "foodItems=";
    var length = foodIdList.length;
    
    for (var i = 0; i < length; i++){
        if(i == 0) urlParam = urlParam + foodIdList[i];
        else urlParam = urlParam + "|" + foodIdList[i];
    }
    
    url = "diet-problem-solver.html?" + urlParam;
    window.location.href = url;
};

$("#optimizeButton").attr("disabled", "disabled");

$("#optimizeButton").click(redirectToSolve);

$("#addAllFoodItems").click(function(){
    var component = $("#nutritionalValuesTable tbody tr");
    for(var i = 0; i < component.length; i++){
        addFoodToList(parseFloat(component[i].getAttribute('id')), $("#nutritionalValuesTable #" + component[i].getAttribute('id') + " .add-button"));
    }
});

$("#removeAllFoodItems").click(function(){
    var tempList = foodIdList.slice(0);
    for(var i = 0; i < tempList.length; i++){
        removeItem(tempList[i]);
    }
});

$(document).on('click', '.panel-heading h3.clickable', function(e){
    var $this = $(this);
    if(!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp("fast");
        $this.addClass('panel-collapsed');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown("fast");
        $this.removeClass('panel-collapsed');
    }
});