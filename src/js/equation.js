var parseObjectiveFunction = function(){
    var inputObjectiveFunction = removeWhiteSpace($("#objectiveFunction").val());
    var extractedEquation = getCoefficients(getLeftHandSide(inputObjectiveFunction));
    return extractedEquation;
};


var parseConstraintFunction = function(){
    var inputConstraintFunctions = $(".constraintFunction");
    
    var minimizationPattern = new RegExp("^(([-+]?(((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)([-+](((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)*[>]=?[-]?((([0-9]+)?\.[0-9]+)|([0-9]*)))$");
    var maximizationPattern = new RegExp("^(([-+]?(((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)([-+](((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)*[<]=?[-]?((([0-9]+)?\.[0-9]+)|([0-9]*)))$");
    
    constraintFunctions = [];
    for(var i = 0; i < inputConstraintFunctions.length; i++){
        var input = removeWhiteSpace(inputConstraintFunctions.get(i).value);
        var inputConstraintFunction = convertInequalityToEquality(input);
        var extractedEquation = getCoefficients(getLeftHandSide(inputConstraintFunction));
        
        if( ($("#optimizationType").val() == "minimize" && maximizationPattern.test(input)) || ($("#optimizationType").val() == "maximize" && minimizationPattern.test(input))){
            extractedEquation.coeff = negateAllCoefficients(extractedEquation.coeff);
            extractedEquation.coeffVar[extractedEquation.coeffVar.length] = "b";
            extractedEquation.coeff[extractedEquation.coeff.length] = -1*parseFloat(getRightHandSide(inputConstraintFunction));
        } else {
            extractedEquation.coeffVar[extractedEquation.coeffVar.length] = "b";
            extractedEquation.coeff[extractedEquation.coeff.length] = parseFloat(getRightHandSide(inputConstraintFunction));
        }

        constraintFunctions[i] = extractedEquation;      
    }
    console.log(constraintFunctions);
    return constraintFunctions;
};


var checkObjectiveFunction = function(){
    var inputObjectiveFunction = removeWhiteSpace($("#objectiveFunction").val());
    var isValid = isObjectiveFunction(inputObjectiveFunction);
    if(!isValid){
        $("#objectiveFunctionHelper").html("Invalid Equation");
        return false;
    } else {
        var extractedEquation = getCoefficients(getLeftHandSide(inputObjectiveFunction));
        if(hasDuplicate(extractedEquation.coeffVar)){
            $("#objectiveFunctionHelper").html("Duplicate variable(s) detected");    
            return false;
        } else if(extractedEquation.coeffVar.indexOf("z") >= 0 || extractedEquation.coeffVar.indexOf("Z") >= 0){
            $("#objectiveFunctionHelper").html("Z and z cannot appear on left hand side");
            return false;
        } else {
            $("#objectiveFunctionHelper").html("");
            return true;
        }
    }
};

var checkAllConstraintFunctions = function(){
    var inputConstraintFunctions = $(".constraintFunction");
    var isValid = true;
    for(var i = 0; i < inputConstraintFunctions.length; i++){
        var input = removeWhiteSpace(inputConstraintFunctions.get(i).value);
        isValid &= checkConstraintFunction(input, $(".constraintFunctionHelper").get(i));
    }

    return isValid;

};

var checkConstraintFunction = function(input, helperComponent){
    var isValid = isConstraintFunction(input, helperComponent);
    if(isValid){
        input = convertInequalityToEquality(input);
        var extractedEquation = getCoefficients(getLeftHandSide(input));
        if(hasDuplicate(extractedEquation.coeffVar)){
            helperComponent.innerHTML = "Duplicate variable(s) detected";  
            return false;  
        } else if(extractedEquation.coeffVar.indexOf("z") >= 0 || extractedEquation.coeffVar.indexOf("Z") >= 0){
            helperComponent.innerHTML = "Z and z are reserved variables"; 
            return false;
        } else {
            helperComponent.innerHTML = "";
            return true;
        }
    } else {
        return false;
    }
};

var isObjectiveFunction = function(equation){
    var pattern = new RegExp("^(([-+]?(((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)([-+](((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)*=[Zz])$");
    return pattern.test(equation);  
};

var isConstraintFunction = function(equation, helperComponent){
    var optimizationType = $("#optimizationType").val();

    var pattern = new RegExp("");
    var minimizationPattern = new RegExp("^(([-+]?(((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)([-+](((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)*[>]=?[-]?((([0-9]+)?\.[0-9]+)|([0-9]*)))$");
    var maximizationPattern = new RegExp("^(([-+]?(((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)([-+](((([0-9]+)?\.[0-9]*[1-9]+[0-9]*)|([1-9][0-9]*)))?[a-zA-Z][a-zA-Z0-9]*)*[<]=?[-]?((([0-9]+)?\.[0-9]+)|([0-9]*)))$");
    
    var isValid = minimizationPattern.test(equation) || maximizationPattern.test(equation);  
    
    if(!isValid){
        helperComponent.innerHTML = "Invalid Equation";
        return false;
    } else {
        helperComponent.innerHTML = "";
        return true;
    }
};



var negateAllCoefficients = function(coefficientList){
    var newCoeff = [];
    for(i = 0; i < coefficientList.length; i++){
        if(coefficientList[i] != 0){
            newCoeff.push(-1*coefficientList[i]);
        } else {
            newCoeff.push(0);
        }
    }
    return newCoeff;
};

var cleanArray = function(arr){
    var cleanArr = [];
    for(var i = 0; i < arr.length; i++){
        if(arr[i] != ""){
            cleanArr[cleanArr.length] = arr[i];
        }
    }   
    return cleanArr;
};

var removeWhiteSpace = function(str){
    return cleanArray(str.split(" ")).join("");
};

var getLeftHandSide = function(equation){
    return equation.split("=")[0];
};


var getRightHandSide = function(equation){
    return equation.split("=")[1];
};


var getCoefficients = function(equation){
    var alphabetPattern = RegExp("^[A-Za-z]$");
    var equation = equation.split("-").join("+-");
    var listOfCoefficients = [];
    var listOfVariableCoefficients = [];
    var terms = cleanArray(equation.split("+"));

    for(var i = 0; i < terms.length; i++){
        for(var j = 0; j < terms[i].length; j++){
            if(alphabetPattern.test(terms[i][j])){
                var coefficients = terms[i].substring(0, j);
                var coefficientsVariables = terms[i].substring(j, terms[i].length);
                
                if(coefficients == "") {
                    coefficients = "1";
                } else if(coefficients == "-") {
                    coefficients = "-1"
                }
                
                break;
            }
        }

        listOfCoefficients[listOfCoefficients.length] = parseFloat(coefficients);
        listOfVariableCoefficients[listOfVariableCoefficients.length] = coefficientsVariables;
    }
    return { "coeff" : listOfCoefficients, "coeffVar" : listOfVariableCoefficients };
}

var convertInequalityToEquality = function(inequality){
    var inequality = removeWhiteSpace(inequality);
    return inequality.split(/[<>=]+/).join("=");
};

var insertSlackVariables = function(matrix){
    var length = matrix.length;
    var rowLength = matrix[0].length;
    for(var i = 0; i < length; i++){
        var bElement = matrix[i][rowLength-1];
        for(var j = 0; j < length; j++){
            if(i == j){
                matrix[i][j+rowLength-1] = 1;
            } else {
                matrix[i][j+rowLength-1] = 0;
            }
        }
        matrix[i][length+rowLength-1] = bElement;
    }

    return matrix;
};

var negateObjectiveFunction = function(matrix){
    var rowLength = matrix[0].length;

    for(var i = 0; i < rowLength-2; i++){
        matrix[matrix.length-1][i] *= -1;
        matrix[matrix.length-1][i] = parseFloat(matrix[matrix.length-1][i]);
    }
    return matrix;
};

