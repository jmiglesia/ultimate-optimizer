var getURLParameters = function(){
    var objectParameter = "{";
    var parameters = window.location.search.substring(1).split('&')
    var length = parameters.length;
    
    for(var i = 0; i < length; i++){
        var queryPair = parameters[i].split("=");
        objectParameter = objectParameter + "\"" + queryPair[0] + "\" : " + "\"" + queryPair[1] + "\"";
        if(i != length-1) objectParameter = objectParameter + ",";
    }

    objectParameter += "}";
    obj = $.parseJSON(objectParameter);
    return obj;
};

var getFoodItemList = function(){
    var param = getURLParameters();
    var foodItems = param.foodItems.split("|");
    var list = [];
    
    for(var i = 0; i < foodItems.length; i++) {
        if(foodItems[i] == "") continue;
        else list.push(parseInt(foodItems[i]));
    }
    
    return list;
};

var foodItems = getFoodItemList();
var MAX_SERVING_FOOD = 10;
var NO_NUTRIENTS = 11;
var NO_SLACK_VARIABLES = 2*NO_NUTRIENTS + foodItems.length + 1;
var MIN_NUTRIENT = [2000, 0, 0, 0, 0, 25, 50, 5000, 50, 800, 10];
var MAX_NUTRIENT = [2250, 300, 65, 2400, 300, 100, 100, 50000, 20000, 1600, 30];
var COST = 99;
var CALORIES = 0; 
var CHOLESTEROL = 1;
var TOTALFAT = 2;
var SODIUM = 3;
var CARBOHYDRATES = 4;
var DIETARYFIBER = 5;
var PROTEIN = 6;
var VITA = 7;
var VITC = 8;
var CALCIUM = 9;
var IRON = 10;

var insertSlackVariables = function(matrix){
    var length = matrix.length;
    var rowLength = matrix[0].length;
    
    for(var i = 0; i < length; i++){
        var bElement = matrix[i][rowLength-1];
        for(var j = 0; j < length; j++){
            if(i == j){
                matrix[i][j+rowLength-1] = 1;
            } else {
                matrix[i][j+rowLength-1] = 0;
            }
        }

        matrix[i][length+rowLength-1] = bElement;
    }

    return matrix;
};

var createMatrixForFoodItems = function(foods){
    var res = [];
    var matrix = [];
    var length = foods.length;
    
    var foodObj = getFoodObjects(foods);
    
    for(var i = 0 ; i < NO_SLACK_VARIABLES; i++){
        var rowValues = [];
        if(i < NO_NUTRIENTS*2){
            if(i == CALORIES  || i == NO_NUTRIENTS + CALORIES ){
                rowValues = getNutrientValues(CALORIES, foodObj);
            } else if(i == CHOLESTEROL || i == NO_NUTRIENTS + CHOLESTEROL ){
                rowValues = getNutrientValues(CHOLESTEROL, foodObj);
            } else if(i == TOTALFAT || i == NO_NUTRIENTS + TOTALFAT ){
                rowValues = getNutrientValues(TOTALFAT, foodObj);
            } else if(i == SODIUM  || i == NO_NUTRIENTS + SODIUM ){
                rowValues = getNutrientValues(SODIUM, foodObj);
            } else if(i == CARBOHYDRATES || i == NO_NUTRIENTS + CARBOHYDRATES ){
                rowValues = getNutrientValues(CARBOHYDRATES, foodObj);
            } else if(i == DIETARYFIBER || i == NO_NUTRIENTS + DIETARYFIBER   ){
                rowValues = getNutrientValues(DIETARYFIBER, foodObj);
            } else if(i == PROTEIN  || i == NO_NUTRIENTS + PROTEIN  ){
                rowValues = getNutrientValues(PROTEIN, foodObj);
            } else if(i == VITA  || i == NO_NUTRIENTS + VITA ){
                rowValues = getNutrientValues(VITA, foodObj);
            } else if(i == VITC  || i == NO_NUTRIENTS + VITC ){
                rowValues = getNutrientValues(VITC, foodObj);
            } else if(i == CALCIUM || i == NO_NUTRIENTS + CALCIUM  ){
                rowValues = getNutrientValues(CALCIUM, foodObj);
            } else if(i == IRON  || i == NO_NUTRIENTS + IRON ){
                rowValues = getNutrientValues(IRON, foodObj);
            }

            if(i == CALORIES || i == CHOLESTEROL || i == TOTALFAT || i == SODIUM || i == CARBOHYDRATES || i == DIETARYFIBER || i == PROTEIN || i == VITA || i == VITC || i == CALCIUM || i == IRON ){
                 for(var ii = 0; ii < rowValues.length; ii++){
                    rowValues[ii] *= -1;
                }
            }
        } else if(i == NO_SLACK_VARIABLES-1){
            rowValues = getNutrientValues(COST, foodObj);
        } else {
            for(var j = 0; j < length; j++){
                if( i-(NO_NUTRIENTS*2) == j ){
                    rowValues[j] = -1;
                } else {
                    rowValues[j] = 0;
                }
            }
        } 

        if(i < NO_NUTRIENTS*2){
            if(i == CALORIES) rowValues[rowValues.length] = -1*MAX_NUTRIENT[CALORIES];
            else if(i == NO_NUTRIENTS + CALORIES) rowValues[rowValues.length] = MIN_NUTRIENT[CALORIES];
            else if(i == CHOLESTEROL) rowValues[rowValues.length] = -1*MAX_NUTRIENT[CHOLESTEROL];
            else if(i == NO_NUTRIENTS + CHOLESTEROL) rowValues[rowValues.length] = MIN_NUTRIENT[CHOLESTEROL];
            else if(i == TOTALFAT) rowValues[rowValues.length] = -1*MAX_NUTRIENT[TOTALFAT];
            else if(i == NO_NUTRIENTS + TOTALFAT ) rowValues[rowValues.length] = MIN_NUTRIENT[TOTALFAT];
            else if(i == SODIUM) rowValues[rowValues.length] = -1*MAX_NUTRIENT[SODIUM];
            else if(i == NO_NUTRIENTS + SODIUM ) rowValues[rowValues.length] = MIN_NUTRIENT[SODIUM];
            else if(i == CARBOHYDRATES) rowValues[rowValues.length] = -1*MAX_NUTRIENT[CARBOHYDRATES];
            else if(i == NO_NUTRIENTS + CARBOHYDRATES )rowValues[rowValues.length] = MIN_NUTRIENT[CARBOHYDRATES];
            else if(i == DIETARYFIBER) rowValues[rowValues.length] =-1*MAX_NUTRIENT[DIETARYFIBER];
            else if(i == NO_NUTRIENTS + DIETARYFIBER )rowValues[rowValues.length] = MIN_NUTRIENT[DIETARYFIBER];
            else if(i == PROTEIN ) rowValues[rowValues.length] = -1*MAX_NUTRIENT[PROTEIN];
            else if(i == NO_NUTRIENTS + PROTEIN ) rowValues[rowValues.length] = MIN_NUTRIENT[PROTEIN];
            else if(i == VITA ) rowValues[rowValues.length] = -1*MAX_NUTRIENT[VITA];
            else if(i == NO_NUTRIENTS + VITA) rowValues[rowValues.length] = MIN_NUTRIENT[VITA];
            else if(i == VITC ) rowValues[rowValues.length] = -1*MAX_NUTRIENT[VITC];
            else if(i == NO_NUTRIENTS + VITC) rowValues[rowValues.length] = MIN_NUTRIENT[VITC];
            else if(i == CALCIUM) rowValues[rowValues.length] = -1*MAX_NUTRIENT[CALCIUM];
            else if(i == NO_NUTRIENTS + CALCIUM) rowValues[rowValues.length] = MIN_NUTRIENT[CALCIUM];
            else if(i == IRON ) rowValues[rowValues.length] = -1*MAX_NUTRIENT[IRON];
            else if(i == NO_NUTRIENTS + IRON ) rowValues[rowValues.length] = MIN_NUTRIENT[IRON];
        } else if (i < NO_SLACK_VARIABLES-1) {
            rowValues[rowValues.length] = -10;
        } else {
            rowValues[rowValues.length] = 0; 
        }

        matrix[i] = rowValues;
                
    }

    return matrix;
};

var getNutrientValues = function(nutritientId, objectArray){
    var nutritient = [];
    for(i = 0; i < objectArray.length; i++){

        switch(nutritientId){
            case CALORIES : nutritient[i] = objectArray[i].calories; break;
            case CHOLESTEROL : nutritient[i] = objectArray[i].cholesterol; break;
            case TOTALFAT : nutritient[i] = objectArray[i].totalFat; break;
            case SODIUM : nutritient[i] = objectArray[i].sodium; break;
            case CARBOHYDRATES : nutritient[i] = objectArray[i].carbohydrates; break;
            case DIETARYFIBER : nutritient[i] = objectArray[i].dietaryFiber;  break;
            case PROTEIN : nutritient[i] = objectArray[i].protein; break;
            case VITA : nutritient[i] = objectArray[i].vitAIU; break;
            case VITC : nutritient[i] = objectArray[i].vitCIU; break;
            case CALCIUM : nutritient[i] = objectArray[i].calcium; break;
            case IRON : nutritient[i] = objectArray[i].iron; break; 
            case COST : nutritient[i] = objectArray[i].pricePerServing; break;   
        }   
    }
    return nutritient;
};

var negateObjectiveFunction = function(matrix){
    var rowLength = matrix[0].length;
    for(var i = 0; i < rowLength-2; i++){
        matrix[matrix.length-1][i] *= -1;
    }

    return matrix;
};

var renderTable = function(tableParam){
    var htmlTable = "<table class='table'>";
    for(var j = 0; j < tableParam[0].length; j++){
        htmlTable = htmlTable + "<td>" +j + "</td>";
    }
    for(var i = 0; i < tableParam.length; i++){
        htmlTable += "<tr>";
        for(j = 0; j < tableParam[i].length; j++){
            htmlTable = htmlTable + "<td>" + tableParam[i][j] + "</td>";
        }
        htmlTable += "</tr>";
    }
    htmlTable += "</table>";
    return htmlTable;
};

var getFoodObjects = function(idList){
    var result = [];
    for(var i = 0; i < idList.length; i++){
        result[i] = lib.query("nutritionalValues", {ID : idList[i]})[0];
    }
    return result;
}

var transposeMatrix = function(a) {
    return Object.keys(a[0]).map(function (c) {
        return a.map(function (r) {
            return r[c];
        });
    });
};

var removeZeroRows = function(matrixParam){
    var matrix = [];
    for(var i = 0; i < matrixParam.length; i++ ){
        var c = 0;
        
        for(var j = 0; j < matrixParam[i].length-1; j++){
            if(matrixParam[i][j] == 0){
                c++;
            }
        }

        if( i == matrixParam.length-1 || (c != matrixParam[i].length-1 && matrixParam[i][matrixParam[i].length-1] != 0)){
            matrix.push(matrixParam[i]);
        }
    }

    return matrix;
};

var normalize =  function(matrixParam){
    for(var i = 0; i < matrixParam.length-1; i++ ){
        if( matrixParam[i][matrixParam[i].length-1] < 0){
            for(var j = 0; j < matrixParam[i].length; j++){
                if(matrixParam[i][j] < 0){
                    matrixParam[i][j] *= -1;
                    
                }
            }
        }

    }
    return matrixParam;
};

var checkIfFeasible = function(foodItems) {
    var foodObj = getFoodObjects(foodItems);
    
    for(var i = 0; i < NO_NUTRIENTS; i++ ){
        var rowValues = getNutrientValues(i, foodObj);
        var sumNutrients = computeSummation(rowValues);
        if( sumNutrients < MIN_NUTRIENT[i] ){
           return false;
        }
    }

    return true;
};

var computeSummation = function(numArr){
    var sum = 0;
    for(var i = 0; i < numArr.length; i++ ){
        sum = sum + MAX_SERVING_FOOD*numArr[i];
    }
    return sum;
};

var createPieChartForPrice = function(){
    var data  = [];
    
    for(var i = 0; i < res.basicSolution[res.basicSolution.length-1].length-1; i++){
        if(res.basicSolution[res.basicSolution.length-1][i][1] > 0){
            var queryResult = lib.query("nutritionalValues", {ID : foodItems[i]})[0];
             var obj = { label : queryResult.foods , data : queryResult.pricePerServing*res.basicSolution[res.basicSolution.length-1][i][1]};
            data.push(obj);
        }
        
    }

    var plotObj = $.plot($("#pricePieChart"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: true
        }
    });
};

var createPieChartForServing = function(){
    var data  = [];
    
    for(var i = 0; i < res.basicSolution[res.basicSolution.length-1].length-1; i++){
        if(res.basicSolution[res.basicSolution.length-1][i][1] > 0){
            var queryResult = lib.query("nutritionalValues", {ID : foodItems[i]})[0];
            var obj = { label : queryResult.foods , data : res.basicSolution[res.basicSolution.length-1][i][1]};
            data.push(obj);
        }
        
    }

    var plotObj = $.plot($("#servingsPieChart"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: true
        }
    });
};

 var createReportHTML = function(foodItems){
    var htmlTable = "<table class='table'>";
        
    htmlTable += "<thead>";
    htmlTable += "<tr>";
    htmlTable = htmlTable + "<th>Foods</th><th>Serving Size</th><th># Servings</th><th>Price per Serving</th><th>Total Price</th>";
    htmlTable += "</tr>";
    htmlTable += "<thead>";
    htmlTable += "<tbody>";
    
    for(var i = 0; i < res.basicSolution[res.basicSolution.length-1].length-1; i++){
        if(res.basicSolution[res.basicSolution.length-1][i][1] > 0){
            var queryResult = lib.query("nutritionalValues", {ID : foodItems[i]})[0];
            htmlTable += "<tr />"; 
            htmlTable = htmlTable + "<td> " + queryResult.foods + "</td>";
            htmlTable = htmlTable + "<td> " + queryResult.servingSize + "</td>";
            htmlTable = htmlTable + "<td> " + res.basicSolution[res.basicSolution.length-1][i][1] + "</td>";
            htmlTable = htmlTable + "<td> $" + queryResult.pricePerServing + "</td>";
            htmlTable = htmlTable + "<td> $" + (res.basicSolution[res.basicSolution.length-1][i][1]*queryResult.pricePerServing) + "</td>";
            htmlTable += "</tr>";
        }
    }
          
    htmlTable += "</tbody>";
    htmlTable = htmlTable + "<tfoot><tr><th></th><th></th><th></th><th>Total Price</th><th>$" + res.basicSolution[res.basicSolution.length-1][i][1] + "</th></tr></tfoot>";
    
    htmlTable += "</table>";
    return htmlTable;

};

var createNotFeasibleHTML = function(foodItems){
    var foodObj = getFoodObjects(foodItems);
    
};


var isFeasible = checkIfFeasible(foodItems);

$("#answer").hide();

if(foodItems.length == 0){   
   $("#summary #message").html("<h2>NO FOODS SELECTED</h2>");
} else if(isFeasible){
    x = createMatrixForFoodItems(foodItems);
    foodVariables = [];

    for(var i = 0; i < foodItems.length; i++){
        foodVariables.push("x" + i);
    }

    foodVariables.push("z");
    x = transposeMatrix(x);
    x = removeZeroRows(x);
    x = insertSlackVariables(x);
    x = negateObjectiveFunction(x);
    
    t = renderTable(x);
    Simplex.prototype = Simplex;
    
    simplex = new Simplex();
    var res = simplex.performSimplexMethod(x, foodVariables, "minimize");
    
    

    if(res.isFeasible){
        var t = createReportHTML(foodItems);
        $("#answer").show();
        $("#solution").append(t);
        
        createPieChartForPrice();
        createPieChartForServing();
        $("#summary #message").html("<h2>Total diet plan costs $" + res.basicSolution[res.basicSolution.length-1][i][1] + "</h2>");
    } else {
        $("#summary #message").html("<h2>NOT FEASIBLE</h2>");
    }
    
} else {
    $("#summary #message").html("<h2>NOT FEASIBLE</h2>");
}

