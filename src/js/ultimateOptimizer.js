pointer = 0;
res = {};

$("#answerDiv").hide();
$("#iterationDiv").hide();

$("#prevIteration").click(function(){
    pointer--;
    var t = createTableauHTML(res.simplexIteration[pointer]);
    $("#tableau table").remove();
    $("#tableau").append(t);
    
    var t = createBasicSolutionHTML(res.simplexIteration[pointer]);
    $("#basicSolution table").remove();
    $("#basicSolution").append(t);

    $("#nextIteration").removeAttr("disabled");
    
    if(pointer == 0){
        $("#prevIteration").attr("disabled", "disabled");
    }

    $("#simplexLabel").html("Simplex Iteration "+ (pointer+1) + "/"+ res.simplexIteration.length);
});

$("#nextIteration").click(function(){
    pointer++;
    var t = createTableauHTML(res.simplexIteration[pointer]);
    $("#tableau table").remove();
    $("#tableau").append(t);
    
    var t = createBasicSolutionHTML(res.simplexIteration[pointer]);
    $("#basicSolution table").remove();
    $("#basicSolution").append(t);

    $("#prevIteration").removeAttr("disabled");
    
    if(pointer == res.simplexIteration.length-1){
        $("#nextIteration").attr("disabled", "disabled");
    }

    $("#simplexLabel").html("Simplex Iteration "+ (pointer+1) + "/"+ res.simplexIteration.length);
});

var hideAnswerPanel = function(){
    $("#answerDiv").hide();
    $("#iterationDiv").hide();
    $("#answerDiv #answer").html("");
    $("#answer table").remove();
};

var checkInputFields = function(){
    if(checkObjectiveFunction() & checkAllConstraintFunctions()){
        
        hideAnswerPanel();

        var OF = parseObjectiveFunction();
        var CF = parseConstraintFunction();
        
        var variables = OF.coeffVar.slice(0);
        variables[variables.length] = "b";
        
        var x = createMatrix(OF, CF, variables);
        
        if($("#optimizationType").val() == "minimize"){
            x = transposeMatrix(copyMatrix(x));
        }

        x = insertSlackVariables(copyMatrix(x));
        x = normalize(copyMatrix(x));
        x = negateObjectiveFunction(copyMatrix(x));
        
        var simplex = new Simplex();
        if($("#optimizationType").val() == "minimize"){
            res = simplex.performSimplexMethod(x, variables, "minimize");
        } else {
            res = simplex.performSimplexMethod(x, variables, "maximize");
        }
        
        if(!res.isFeasible){
            $("#answerDiv").show();
            $("#answerDiv #answer").append("NOT FEASIBLE");
        } else {
            res.variables = variables;

            $("#answerDiv").show();
            $("#iterationDiv").show();

            t = createAnswerHTML();
            $("#answer table").remove();
            $("#answer").append(t);


            pointer = 0;
            $("#prevIteration").attr("disabled", "disabled");
            $("#nextIteration").removeAttr("disabled");
            
            var t = createTableauHTML(res.simplexIteration[pointer]);
            $("#tableau table").remove();
            $("#tableau").append(t);
            $("#simplexLabel").html("Simplex Iteration "+ (pointer+1) + "/"+ res.simplexIteration.length);          
            
            t = createBasicSolutionHTML(res.simplexIteration[pointer]);
            $("#basicSolution table").remove();
            $("#basicSolution").append(t);
        }
    } else {
        console.log("dasdasd");
    }
};



var addConstraintInputField = function(thisComponent){
    length = $("#constraintFunction").children().length;
    newInputField = "<div class=\"form-group\" ><label for=\"type\" class=\"col-sm-3 control-label\"></label> <div class=\"form-inline col-sm-6\"> <input type=\"text\" class=\"form-control constraintFunction\" name=\"constraintFunction\" required/> <button type=\"button\" onclick=\"addConstraintInputField($(this))\" class=\"btn btn-default\">+</button> <button type=\"button\" onclick=\"removeConstraintInputField($(this))\" class=\"btn btn-default\">x</button></div><span class=\"constraintFunctionHelper\"> </span></div>";
   $(newInputField).insertAfter(thisComponent.parent().parent());
};

var removeConstraintInputField = function(thisComponent){
    thisComponent.parent().parent().remove();
};