Simplex = function(){
    this.simplexMatrix = [];
    this.matrixVariables = [];
    this.checkIfStop = function(){
        var counter = 0;
        for(var i = 0; i < this.simplexMatrix[this.simplexMatrix.length-1].length-2; i++){
            var element = this.simplexMatrix[this.simplexMatrix.length-1][i];
            if(element >= 0) {
                counter++;
            }
        }
        
        if(counter == this.simplexMatrix[this.simplexMatrix.length-1].length-2){
            return true;
        } else {
            return false;
        }
        
    };
    
    this.selectPivotColumn = function(){
        var maximunNegative = 0;
        var pivotColumn = -1;
        for(var i = 0; i <this. simplexMatrix[this.simplexMatrix.length-1].length-2; i++){
            element = this.simplexMatrix[this.simplexMatrix.length-1][i];
            if( element < 0 && element <= maximunNegative){
                maximunNegative = element;
                pivotColumn = i;
            }
        }
        return pivotColumn;
    };
    
    this.selectPivotRow = function(pivotColumn){
        var minimumRatio = 999999999999999;
        var pivotRow = -1;
        for(var i = 0; i < this.simplexMatrix.length-1; i++){
            var columnElement = this.simplexMatrix[i][pivotColumn];
            if( columnElement > 0 ){
                ratio = this.simplexMatrix[i][this.simplexMatrix[this.simplexMatrix.length-1].length-1] / columnElement;
                if(ratio <= minimumRatio){
                    minimumRatio = ratio;
                    pivotRow = i;
                }
            }
        }
        return pivotRow;
    };
    
    this.normalizePivotRow = function(pivotRow, pivotColumn){
        var pivotElement = this.simplexMatrix[pivotRow][pivotColumn];
        
        for(var i = 0; i < this.simplexMatrix[this.simplexMatrix.length-1].length; i++){
            this.simplexMatrix[pivotRow][i] /= pivotElement;
        }
    };
    
    this.peformGaussJordanSingleIteration = function(pivotRow, pivotColumn){
        for(var i = 0; i < this.simplexMatrix.length; i++){
            if(i != pivotRow){
            
                var multiplier = this.simplexMatrix[i][pivotColumn]/this.simplexMatrix[pivotRow][pivotColumn];
            
                for(var j = 0; j < this.simplexMatrix[i].length; j++){
                    this.simplexMatrix[i][j] -= (multiplier*this.simplexMatrix[pivotRow][j]);
                }
            }
        }              
    };
        
    this.updateBasicSolution = function(option){
        if(option == "maximize"){
            return this.maximizationBasicSolution();
        } else {
            return this.minimizationBasicSolution();
        }
    }
    
    this.maximizationBasicSolution = function(){
        var tableau = [];
        var k = 0;
        for(j = 0; j < this.simplexMatrix[0].length-1; j++){
            var counterNonZero = 0;
            var pointerCounterNonZero = -1;
            var row = [];
            for(i = 0; i < this.simplexMatrix.length; i++){
                if(this.simplexMatrix[i][j] != 0){
                    counterNonZero++;
                    pointerCounterNonZero = i;
                }
            }
            
            if(j < this.matrixVariables.length-1){
                row.push(this.matrixVariables[j]);
            } else if(j <  this.simplexMatrix[0].length-2){
                row.push("s"+k);   
                k++;
            } else {
                row.push("z");
            }

            if(counterNonZero == 1){
                row.push(this.simplexMatrix[pointerCounterNonZero][this.simplexMatrix[0].length-1]);
            } else {
                row.push(0);
            }

            tableau.push(row);
        }
        
        return tableau;
    };

    this.minimizationBasicSolution = function(){
        var tableau = [];
        var row = [];

        for(var i = this.simplexMatrix[0].length-this.matrixVariables.length-1; i < this.simplexMatrix[0].length-2; i++){
            row.push(this.matrixVariables[Math.abs(this.simplexMatrix[0].length-this.matrixVariables.length-1-i)]);
            row.push(Math.abs(this.simplexMatrix[this.simplexMatrix.length-1][i]));
            tableau.push(row);
            row = [];
        }

        row.push("z");
        row.push(this.simplexMatrix[this.simplexMatrix.length-1][this.simplexMatrix[0].length-1]);
        tableau.push(row);
        return tableau;
    };

    this.copyMatrix = function(matrixParam){
        var copy = [];
        for(var i = 0; i < matrixParam.length; i++){
            var r = [];
            for(var j = 0; j < matrixParam[i].length; j++){
                r.push(matrixParam[i][j]);
            }
            copy.push(r);
        }
        return copy;
    }

    this.createVariables = function(option){
        if(option == "maximize"){
            return this.createVariablesForMaximization();
        } else {
            return this.createVariablesForMinimization();
        }
        
    };

    this.createVariablesForMaximization = function(){
        var variables = [];
        for(var i = 0; i < this.matrixVariables.length-1; i++){
            variables.push(this.matrixVariables[i]);
        }

        for(var i = 0; i < this.simplexMatrix.length-1; i++){
            variables.push("s"+i);
        }

        variables.push("z");
        variables.push("b");

        return variables;
    };

    this.createVariablesForMinimization = function(){
        var variables = [];

        for(var i = 0; i < this.simplexMatrix[0].length-this.matrixVariables.length-1; i ++){
            variables.push("y"+i);
        }

        for(var i = 0; i < this.matrixVariables.length-1; i++){
            variables.push(this.matrixVariables[i]);
        }

        variables.push("z");
        variables.push("b");
        
        return variables;
    };

    this.performSimplexMethod = function(simplexMatrixParam, variables, option){
        this.simplexMatrix = this.copyMatrix(simplexMatrixParam);
        this.matrixVariables = variables;
        var process = [];
        var basicSolution = [];
        
        while(!this.checkIfStop()){
            process.push(this.copyMatrix(this.simplexMatrix));
            basicSolution.push(this.updateBasicSolution(option));
            
            var pivotColumn = this.selectPivotColumn();
            var pivotRow = this.selectPivotRow(pivotColumn);
            
            if(pivotRow < 0) {
                return { "isFeasible" : false };
            }

            this.normalizePivotRow(pivotRow, pivotColumn);
            this.peformGaussJordanSingleIteration(pivotRow, pivotColumn);
        }
        
        process.push(this.copyMatrix(this.simplexMatrix));
        basicSolution.push(this.updateBasicSolution(option));
        var labelVariables = this.createVariables(option);
        return { "isFeasible" : true, "simplexIteration" : process, "basicSolution": basicSolution, "headerVariables" : labelVariables, "variables" : variables };
    };
}